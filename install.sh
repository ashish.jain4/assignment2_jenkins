#!/bin/bash
#package_name=$1
pkg=$1
yum list installed ${pkg}

if [ ! $? = 0 ]; then
   sudo yum install $pkg -y
else
  echo "'${pkg}' is already in your system"
fi
